# Lichtwecker selber bauen

## Vorgaben
* Kosten < 50,-€
* Die Whg im Hinterhof ist dunkel, hell muss es sein
* Licht für morgens und abends, also Sonnenauf und -untergang
* kein IOT-Zeugs
* einfach zu bauen

## Fragen
* Wie steuern, Arduino, ...
* Leuchtmittel, LED-Streifen, Glühbirne, es soll ja schon hell sein, ein einfacher LED-Streifen reicht dafür bestimmt nicht
* LED-Controller, was gibt es da 
* Form / Gehäuse, zum hinstellen, an die Decke hängen, ...
* Wie hell muss es denn sein?
* Licht, wie wird das nochmal quantifiziert, es gibt Lux, dann noch die Farbe,...

## Ressourcen
* Conrad
* Reichelt
* Ebay
* Pollin

## Linksammlung
  * https://alexbloggt.com/diy-lichtwecker-1/
  * https://sites.google.com/site/fpgaandco/sunrise
  * https://www.raspberrypi.org/blog/circadia-sunrise-lamp/
  * https://www.electroschematics.com/diy-sunrise-alarm-using-arduino/
  * https://www.instructables.com/id/Sunrise-Alarm-Clock-With-Arduino/
  * Elektronik Bastelseite mit Beispielen [bestengineeringprojects.com](https://bestengineeringprojects.com/sound-activated-0-30-minutes-timer-circuit/)

### Blogs
  * https://mjrobot.org/

### Das haben andere schon gemacht
  * https://www.electroschematics.com/diy-sunrise-alarm-using-arduino/
  * 


### Arduino und Zeit
  * https://arduino-hannover.de/2012/06/14/dcf77-empfanger-mit-arduino-betreiben/
  * Zeit für Arduino,  https://www.pgollor.de/cms/?page_id=764

### Arduino und Elektronik
  * https://bestengineeringprojects.com/arduino-mosfet-led-driver-circuit/
  * Einfacher Spannungsregler: [Anleitung](https://starthardware.org/spannungsregler-2/)



### Micropython
  * Micropython und Jupyter Notebook [link](https://towardsdatascience.com/micropython-on-esp-using-jupyter-6f366ff5ed9)
  * Awesome List for Micropython bei [GitHub](https://github.com/pfalcon/awesome-micropython)
  * Uhrzeit für den Sonnenaufgang [link](https://stackoverflow.com/questions/19615350/calculate-sunrise-and-sunset-times-for-a-given-gps-coordinate-within-postgresql)

### Kaufen
  * LED-Streifen bei [Aliexpress](https://www.aliexpress.com/item/32349514234.html)

## Notizen
  * MicroPython wär cool
  * LED Streifen sind gleub ich das Mittel der Wahl. Durch Massenproduktion gibt es Streifen und Controller wahrscheinlich recht günstig
  * RGB wäre auch besser, für die Stimmung
  * Steuerung per Arduino mit Mosefets
  * Keine Leisten, Alu schienen mit Led's drauf z.B. bei [led-tech](https://www.led-tech.de/de/lineare-led-module)
  * 12/24V Netzteil ~60W
  * Freakshow, auch mit Thema LED [link](https://freakshow.fm/fs244-ottilie-normalcamperin)
 

## Kosten
- Arduino - hab ich                          0,-
- Netzteil - hab ich                         0,-
- Uhr RTC-Modul                              4,-
- Gehäuse - diy                              0,-
- LED Leuchtmittel     ...
- Mosfet IRLZ44N                             1,-
- Widerstände 100Ohm/120Ohm, 10K
- Spannungsversorgung (bei 24V/12 LED-Modul) 4,-
- ESP8266 auf NodeMCU als Gehirn             9,-
                                           -----
                                            18,-

wobei ich beim ESP keine zusätzliche RTC brauche, 4– fallen also weg, die Spannungsversorgung brauche ich erstmal auch nicht, viell. gibts einen


